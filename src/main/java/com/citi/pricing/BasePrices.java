package com.citi.pricing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

public class BasePrices
    extends HashMap<String,Double>
{
    public BasePrices(String filename) {
        try {
            Files.lines (Paths.get (filename)).forEach(line -> {
                    String[] pair = line.split(",");
                    put(pair[0], Double.parseDouble(pair[1]));
                });
        } catch(IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        BasePrices basePrices = new BasePrices("/app/BasePrices.csv");
        System.out.println("AAPL: " + basePrices.get ("AAPL"));
    }
}
